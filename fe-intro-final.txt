1. - Nama domain adalah serangkaian teks yang dipetakan ke alamat IP alfanumerik, digunakan untuk mengakses situs web dari perangkat lunak klien
    - Nama domain adalah rangkaian teks yang dipetakan ke alamat IP alfanumerik, sedangkan url berisi nama domain situs serta informasi lainnya, termasuk protokol dan jalurnya
2. - Web hosting adalah layanan online yang membuat konten situs web Anda dapat diakses di internet.
   - Host web menyediakan teknologi hosting dan sumber daya yang diperlukan untuk pengoperasian situs web Anda yang efektif dan aman. Mereka bertanggung jawab untuk menjaga dan menjalankan server, menerapkan langkah-langkah keamanan hosting, dan memastikan bahwa data seperti teks, foto, dan file lainnya berhasil ditransfer ke browser pengunjung.
3. - Sistem Nama Domain (DNS) adalah buku telepon Internet
   - Proses resolusi DNS melibatkan konversi nama host (seperti www.example.com) menjadi alamat IP yang ramah komputer (seperti 192.168.1.1). Alamat IP diberikan kepada setiap perangkat di Internet, dan alamat tersebut diperlukan untuk menemukan perangkat Internet yang sesuai - seperti alamat jalan yang digunakan untuk menemukan host tertentu.
4. - Untuk memahami proses di balik resolusi DNS, penting untuk mempelajari berbagai komponen perangkat keras yang harus dilewati oleh reuqest DNS. Untuk browser web, pencarian DNS terjadi "di belakang layar" dan terjadi setelah ada trigger dari user pada browser.
5. - sebuah cara bagaimana kita menemukan sebuah lokasi file yang berada di internet, Baik berupa situs, gambar, video, program perangkat lunak, ataupun jenis file lainnya yang di-host di server.
6. - Pengguna lebih mudah mengingat nama domain dibanding IP 
   - lebih bflexible dengan DNS dbanding alamat IP, server dapat diubah tanpa mempengaruhi pengguna.

7. - Domain Name adalah alamat unik yang digunakan untuk mengidentifikasi sebuah situs web di internet sedangkan Browser adalah aplikasi perangkat lunak yang digunakan untuk mengakses, mengambil, dan menampilkan konten web.
   - fungsi domain name : pemetaan IP, untuk identifikasi yang unik dan mudah di kenali
     fungsi browser: Rendering Halaman Web, Navigasi, Bookmarks and History, search konten, GUI untuk web, managemen konten web, security web dll
8. - Browser bertanggung jawab untuk mengambil dan menampilkan konten web kepada pengguna. Saat pengguna memasukkan URL atau mengeklik tautan, browser memulai serangkaian tindakan kompleks untuk mengambil konten web dari server dan menampilkannya di perangkat pengguna.
9. - Web hosting adalah layanan online yang memungkinkan Anda mempublikasikan situs web atau aplikasi web Anda di internet. Saat Anda mendaftar ke layanan hosting web, pada dasarnya Anda menyewa beberapa ruang di server fisik tempat Anda dapat menyimpan semua file dan data yang diperlukan agar situs web Anda berfungsi dengan baik.
   - Penyedia layanan web hosting memastikan situs web Anda bekerja secara optimal dan dengan protokol keamanan yang lebih baik. Selain itu, ini menyederhanakan banyak aspek kompleks dalam hosting situs web – mulai dari instalasi perangkat lunak hingga dukungan teknis.
10. sebagai pondasi awal agar paham jika terjadi kendala pada website, web hosting atau pung browser sehingga mudah untuk proses mitigasi masalah. karena kendala pada web bukan hanya dari sisi web tapi 1 rangkai penuh.
